package com.example.demo.PkgClases.Vista;

import com.example.demo.PkgClases.Filtro;
import com.example.demo.PkgClases.FiltroRep;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.management.*;
import javax.management.Notification;
import javax.swing.*;

/**
 * Created by emr on 20/07/2017.
 */
@SpringUI
public class principal extends UI{
    @Autowired
    FiltroRep filtroRepository;
    @Override
    protected void init(VaadinRequest vaadinRequest){
        VerticalLayout layoutV= new VerticalLayout();
        HorizontalLayout layoutH= new HorizontalLayout();
       // TextField codigo = new TextField("Codigo");
        TextField estado = new TextField("Estado");
        TextField presion = new TextField("PresionAgua");
        TextField clasificaion = new TextField("Clasificacion");
        Button agregar = new Button("Agregar");
        Grid<Filtro> tbl = new Grid<>();
        tbl.addColumn(Filtro::getId).setCaption("Codigo");
        tbl.addColumn(Filtro::getEstado).setCaption("Estado");
        tbl.addColumn(Filtro::getPresionAgua).setCaption("PresionAgua");
        tbl.addColumn(Filtro::getClasificacion).setCaption("Clasificacion");
        agregar.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
             Filtro f = new Filtro();
            // f.setCodigo(Integer.parseInt(codigo.getValue()));
             f.setEstado(estado.getValue());
             f.setPresionAgua(Double.parseDouble(presion.getValue()));
             f.setClasificacion(clasificaion.getValue());
             filtroRepository.save(f);
             tbl.setItems(filtroRepository.findAll());
            // codigo.clear();
             estado.clear();
             presion.clear();
             clasificaion.clear();
          // Notification.Show("Registro de filtro agregado");

         }

        });

layoutH.addComponents(estado, presion, clasificaion,agregar);
layoutH.setComponentAlignment(agregar, Alignment.BOTTOM_RIGHT );
layoutV.addComponents(layoutH,tbl);
setContent(layoutV);
    }
}
