package com.example.demo.PkgClases;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by emr on 20/07/2017.
 */
public interface FiltroRep extends JpaRepository<Filtro,Long> {
}
