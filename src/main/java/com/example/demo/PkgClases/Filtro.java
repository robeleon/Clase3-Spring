package com.example.demo.PkgClases;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

/**
 * Created by emr on 20/07/2017.
 */
@Entity
public class Filtro {
    private int codigo;
    private String estado;
    private Double presionAgua;
    private String Clasificacion;
@Id@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Filtro() {
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Double getPresionAgua() {
        return presionAgua;
    }

    public void setPresionAgua(Double presionAgua) {
        this.presionAgua = presionAgua;
    }

    public String getClasificacion() {
        return Clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        Clasificacion = clasificacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
